FROM python:3.7.13-alpine3.14
LABEL Maintainer="Iago Pattas"

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN python -m pip install -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./app /app

RUN adduser -D main 
USER main




